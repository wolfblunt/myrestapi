from flask import Flask
from flask import request
import pymongo
import json
import configparser

configfile = configparser.ConfigParser()
configfile.read(r'settings.conf')
h = configfile.get("mongodb", "host")
u = configfile.get("mongodb", "user")
p = int(configfile.get("mongodb", "port"))
d = configfile.get("mongodb", "dbname")
c = configfile.get("mongodb", "collectionName")

app = Flask(__name__)
client = pymongo.MongoClient(host=h, port=p)

db = client[d]
collection = db[c]


# To insert new user document in record.
@app.route("/", methods=['POST'])
def insert_document():
    try:
        # connection = getDbConnection()
        req_data = request.get_json()
        collection.insert_one(req_data)

        # closeDbConnection(connection)
        return ('', 204)
    except Exception as e:
        print("Error", e)
    finally:
        client.close()


# To fetch all the records from the database.
@app.route('/listAll')
def get():
    try:
        # connection = getDbConnection()
        db = client[d]
        collection = db[c]
        documents = collection.find()
        response = []
        for document in documents:
            document['_id'] = str(document['_id'])
            response.append(document)

        # closeDbConnection(connection)
        return json.dumps(response)
    except Exception as e:
        print("Error", e)
    finally:
        client.close()


# To fetch the details for a particular user on the basis of its id.
@app.route('/detail/<int:id>')
def getUsingID(id):
    try:
        u_id = id
        print(type(id))
        documents = collection.find({"userid": u_id})
        print(documents)
        response = []
        for document in documents:
            value = id
            print(value)
            document['_id'] = value
            response.append(document)
        print(response)
        return json.dumps(response)
    except Exception as e:
        print("Error", e)
    finally:
        client.close()


# To update the record
@app.route('/update/<int:id>', methods=['POST'])
def upadteUsingID(id):
    try:
        req_data = request.get_json()
        u_id = id
        documents = collection.find({"userid": u_id})
        print(documents)
        for document in documents:
            if document["userid"] == id:
                collection.update_many(
                    {"userid": id},
                    {
                        "$set": req_data

                    }
                )

        return "Update Successful"
    except Exception as e:
        print("Error", e)
    finally:
        client.close()


if __name__ == '__main__':
    app.run(debug=True)  # run on localhost:5000
